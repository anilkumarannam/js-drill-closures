const limitFunctionCallCount = (callBackFunction, n) => {
    let count = n;
    const innerFunction = () => {
        if (count > 0) {
            count--;
            return callBackFunction();
        } else {
            return null;
        }
    }
    return innerFunction;
}

module.exports = limitFunctionCallCount;