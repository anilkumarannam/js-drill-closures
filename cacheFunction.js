const cacheFunction = (callBackFunction) => {
    const cacheObject = {};

    const innerFunction = (...params) => {
        if (params) {
            let paramsKey = JSON.stringify(params);
            if (!(paramsKey in cacheObject)) {
                cacheObject[paramsKey] = callBackFunction(...params);
            }
            return cacheObject[paramsKey];
        }
    }
    return innerFunction;
}

module.exports = cacheFunction;