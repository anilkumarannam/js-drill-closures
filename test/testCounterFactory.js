const counterFactory = require("../counterFactory");

const { increment, decrement } = counterFactory();

console.log(increment());  // Returns 1
console.log(increment());  // Returns 2
console.log(increment());  // Returns 3
console.log(increment());  // Returns 4
console.log(decrement());  // Returns 3
console.log(decrement());  // Returns 2
console.log(decrement());  // Returns 1