const limitFunctionCallCount = require("../limitFunctionCallCount");


const callBackFunction = () => {
    return "Hi";
}

const innerFunction = limitFunctionCallCount(callBackFunction, 3);

console.log(innerFunction()); // Returns Hi
console.log(innerFunction()); // Returns Hi
console.log(innerFunction()); // Returns Hi
console.log(innerFunction()); // Returns null
console.log(innerFunction()); // Returns null
console.log(innerFunction()); // Returns null
console.log(innerFunction()); // Returns null
console.log(innerFunction()); // Returns null