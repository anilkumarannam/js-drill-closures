const cacheFunction = require("../cacheFunction");

const callBackFunction = (...parameters) => {
    return ["Cache CB", ...parameters];
}

const innerFunction = cacheFunction(callBackFunction);

console.log(innerFunction());            // Returns ['Cache CB']
console.log(innerFunction(1, 2));        // Returns ['Cache CB', 1, 2 ]
console.log(innerFunction(2, 1));        // Returns ['Cache CB', 2, 1 ]
console.log(innerFunction(1, 2));        // Returns ['Cache CB', 1, 2 ]
console.log(innerFunction(4));           // Returns ['Cache CB', 4 ]
console.log(innerFunction(5));           // Returns ['Cache CB', 5 ]
console.log(innerFunction(5));           // Returns ['Cache CB', 5 ]
console.log(innerFunction({ a: 'a' }));  // Returns ['Cache CB', {a: 'a'} ]
console.log(innerFunction({ a: 'a' }));  // Returns ['Cache CB', {a: 'a'} ]